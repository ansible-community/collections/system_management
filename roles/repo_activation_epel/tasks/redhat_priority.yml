---

- name: "Check if epel priority is lower than original repo priority"
  ansible.builtin.fail:
    msg: |-
      Priority of epel repo: {{ repo_activation_epel_epel_priority }} is lower than priority
      of original repo: {{ repo_activation_epel_original_repo_priority }}
  when: (repo_activation_epel_original_repo_priority | int) >= (repo_activation_epel_epel_priority | int)

- name: "Activate RHEL 7 optional repo"
  community.general.rhsm_repository:
    name: "{{ repo_activation_epel_optional_repo_name }}"
    state: enabled
  when: ansible_distribution_major_version | int == 7

- name: "Install yum-plugin-priority for RHEL 7"
  ansible.builtin.package:
    name: "yum-plugin-priorities"
    state: present
  when: ansible_distribution_major_version | int == 7

- name: "Retrieve remote repository file"
  ansible.builtin.fetch:
    src: "{{ repo_activation_epel_redhat_original_repo_file }}"
    dest: "{{ repo_activation_epel_redhat_repo_copy_location }}"
    flat: true
  changed_when: false

- name: "Get all repos for rhel systems from satellite: rhel generic, satellite-tools, codeready"
  ansible.builtin.shell:
    cmd: egrep -i '^\[rhel|^\[satellite|^\[code' {{ repo_activation_epel_redhat_original_repo_file }} | sed 's,.\(.*\).$,\1,g'
    executable: /bin/bash
  register: repo_activation_epel_all_repo_sections
  changed_when: false

- name: "Get all EPEL repos for rhel systems from satellite"
  ansible.builtin.shell:
    cmd: grep -i '^\[.*epel' {{ repo_activation_epel_redhat_original_repo_file }} | sed 's,.\(.*\).$,\1,g'
    executable: /bin/bash
  register: repo_activation_epel_epel_sections
  changed_when: false

- name: Set all enabled and priority repos
  ansible.builtin.set_fact:
    repo_activation_epel_all_original_enabled_no_priority_repos: |-
      {%- set enabled_no_priority_repos = [] -%}
      {%- for repo_name in repo_activation_epel_all_repo_sections.stdout_lines -%}
        {%- set enabled=lookup('ansible.builtin.ini', 'enabled', section=repo_name, file=repo_activation_epel_redhat_repo_copy_location) -%}
        {%- set priority=lookup('ansible.builtin.ini', 'priority', section=repo_name, file=repo_activation_epel_redhat_repo_copy_location) -%}
        {%- if enabled == "1" and priority == "" -%}
          {%- set x=enabled_no_priority_repos.append(repo_name) -%}
        {%- endif -%}
      {%- endfor -%}
      {{ enabled_no_priority_repos }}
    repo_activation_epel_all_epel_enabled_no_priority_repos: |-
      {%- set enabled_no_priority_repos = [] -%}
      {%- for repo_name in repo_activation_epel_epel_sections.stdout_lines -%}
        {%- set enabled=lookup('ansible.builtin.ini', 'enabled', section=repo_name, file=repo_activation_epel_redhat_repo_copy_location) -%}
        {%- set priority=lookup('ansible.builtin.ini', 'priority', section=repo_name, file=repo_activation_epel_redhat_repo_copy_location) -%}
        {%- if enabled == "1" -%}
          {%- set x=enabled_no_priority_repos.append(repo_name) -%}
        {%- endif -%}
      {%- endfor -%}
      {{ enabled_no_priority_repos }}

- name: Remove original.repo file from tmp directory
  ansible.builtin.file:
    path: "{{ repo_activation_epel_redhat_repo_copy_location }}"
    state: absent
  delegate_to: 127.0.0.1
  changed_when: false

- name: 'Ensure "priority={{ repo_activation_epel_original_repo_priority }}" is in enabled rhel satellite repositories in specified file'
  community.general.ini_file:
    path: "{{ repo_activation_epel_redhat_original_repo_file }}"
    section: "{{ item }}"
    option: priority
    value: "{{ repo_activation_epel_original_repo_priority }}"
    mode: '0644'
  loop: "{{ repo_activation_epel_all_original_enabled_no_priority_repos }}"

- name: 'Ensure "priority={{ repo_activation_epel_epel_priority }}" is in enabled epel rhel satellite repositories in specified file'
  community.general.ini_file:
    path: "{{ repo_activation_epel_redhat_original_repo_file }}"
    section: "{{ item }}"
    option: priority
    value: "{{ repo_activation_epel_epel_priority }}"
    mode: '0644'
  loop: "{{ repo_activation_epel_all_epel_enabled_no_priority_repos }}"
