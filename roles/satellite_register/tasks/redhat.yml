---

- name: Configure OS specific variables
  ansible.builtin.include_vars: '{{ ansible_os_family | lower }}.yml'
  ignore_errors: true

- name: "Get different Satellite configuration to choose from"
  ansible.builtin.set_fact:
    sat_configuration_names: >-
      {%- set sat_config_names = [] -%}
      {%- for sat in sat_configurations -%}
        {%- set x=sat_config_names.append(lookup('dict', sat).key) -%}
      {%- endfor -%}
      {{ sat_config_names }}

- name: Check if Satellite configuration is not configured
  ansible.builtin.fail:
    msg: "The Satellite configuration: {{ sat_config }} is not available. Set the variable sat_config to one of the following: {{ sat_configuration_names | join(', ') }}"
  when: sat_config not in sat_configuration_names

- name: "Get faulty information of desired configuration and filtered structure"
  ansible.builtin.set_fact:
    sat_configuration_active: >-
      {%- for sat in sat_configurations -%}
        {%- if sat_config == lookup('dict', sat).key -%}
          {{ sat }}
        {%- endif -%}
      {%- endfor -%}
    sat_configurations_inactive: >-
      {%- set sat_config_inactive = [] -%}
      {%- for sat in sat_configurations -%}
        {%- if sat_config != lookup('dict', sat).key -%}
          {%- set x=sat_config_inactive.append(sat) -%}
        {%- endif -%}
      {%- endfor -%}
      {{ sat_config_inactive }}

- name: Read name of current Satellite
  ansible.builtin.shell: set -o pipefail && /usr/sbin/subscription-manager config --list|egrep "^\s+hostname" | cut -d '=' -f 2 | cut -d " " -f 2
  register: current_server
  changed_when: false
  ignore_errors: true

- name: Unregister if Satellite server has changed
  community.general.redhat_subscription:
    state: absent
  when: lookup('dict', sat_configuration_active).value.fqdn != current_server.stdout

- name: Remove inactive Satellite certificates
  ansible.builtin.package:
    state: absent
    name: "katello-ca-consumer-{{ lookup('dict', sat).value.fqdn }}"
  loop: "{{ sat_configurations_inactive }}"
  loop_control:
    label: "katello-ca-consumer-{{ lookup('dict', sat).value.fqdn }}"
    loop_var: sat
  ignore_errors: true

- name: "Install active Satellite certificate for Satellite server: {{ lookup('dict', sat_configuration_active).value.fqdn }}"
  ansible.builtin.command:
    cmd: "/usr/bin/rpm -Uvh http://{{ lookup('dict', sat_configuration_active).value.fqdn }}/pub/katello-ca-consumer-latest.noarch.rpm"
    creates: /usr/bin/katello-rhsm-consumer
    warn: false

- name: "Ensure hostname={{ lookup('dict', sat_configuration_active).value.fqdn }} is in the Satellite config file /etc/rhsm/rhsm.conf"
  ansible.builtin.ini_file:
    path: /etc/rhsm/rhsm.conf
    mode: 0644
    section: server
    option: hostname
    value: "{{ lookup('dict', sat_configuration_active).value.fqdn }}"

- name: "Ensure baseurl=https://{{ lookup('dict', sat_configuration_active).value.fqdn }}/pulp/repos is in the Satellite config file /etc/rhsm/rhsm.conf"
  ansible.builtin.ini_file:
    path: /etc/rhsm/rhsm.conf
    mode: 0644
    section: rhsm
    option: baseurl
    value: "https://{{ lookup('dict', sat_configuration_active).value.fqdn }}/pulp/repos"

- name: Check if already registered
  ansible.builtin.command: /usr/sbin/subscription-manager status
  register: sat_subscribed
  ignore_errors: true
  changed_when: false

- name: Clean registration if registration status is unkown
  ansible.builtin.command: /usr/sbin/subscription-manager clean
  ignore_errors: true
  when: sat_subscribed.rc != 0

- name: Register system to Satellite forcefully
  community.general.redhat_subscription:
    server_hostname: "{{ lookup('dict', sat_configuration_active).value.fqdn }}"
    activationkey: "{{ sat_activationkey }}"
    org_id: "{{ sat_organization }}"
    force_register: true
  when:
    - sat_subscribed.rc != 0 or sat_register_force

# Ansible run may fail if Ansible is updated during run, so it is excluded here.
- name: Update all packages
  ansible.builtin.yum:
    name: "*"
    state: latest
    exclude: ansible
  when:
    - sat_update_all
    - ansible_distribution_major_version | int == 7

- name: Update all packages
  ansible.builtin.dnf:
    name: "*"
    state: latest
    exclude: ansible
  when:
    - sat_update_all
    - ansible_distribution_major_version | int > 7
