System Update Manager
=========

Update systems once or regularly with the standard system package manager, 
and maintain the package exclusion list.

Distributions tested with Molecule:

 - CentOS 7/8
 - Debian 10 Buster
 - Ubuntu 20.04


Role Variables
--------------

 - system_update_manager_excluded: list of packages to be excluded from updates
 - system_update_manager_forceupdate: perform an upgrade now
 - system_update_manager_autoreboot: determine if the system has to be rebooted, and perform reboot
 - system_update_manager_cronjob: install a update job to cron
 - system_update_manager_cron_schedule: schedule in cron format, i.e. `"0 3 * * *"`
 - system_update_manager_cron_force_reboot: add an automatic reboot after updates triggered by cron
