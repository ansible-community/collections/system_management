Ansible Job Installation
========================

This role will call an `ansible-pull` which
- runs at the end of this role
- runs at every reboot
- is executed regularly

These modes may be combined as desired. 

Distributions tested with Molecule:

 - CentOS 7/8
 - Debian 10 Buster
 - Ubuntu 20.04


Usage
-----

- ansible_pull_repository: by default, this variable is empty. A git URL must be supplied, either:
  - to a public repo URL
  - to a private URL, with an access key, i.e. https://oauth2:<gitlab_access_key>@gitlab.ethz.ch/id-cd-lnx/ansible/id-cd-lnx.git
- ansible_pull_inventory: defaults to "inventory"
- ansible_pull_playbook: defaults to "site.yml"
- ansible_pull_tags: defaults to empty
- ansible_pull_checkout: defaults to "master", may be a tag, branch or commit
- ansible_pull_sleep: if you install a cronjob, this parameter will randomly
  delay the configuration run to reduce the load on the git server. Defaults
  to 1740, which means the run will be delayed by 0 to 1740 seconds. Should be
  near the interval of the cronjob.
- ansible_pull_cron_user: user to run the cronjob (default: root)
- ansible_pull_cron_schedule: cron job schedule, default '0/30 * * * *'
- ansible_pull_install_postboot: install a job which executes ansible-pull at
  the end of a system boot (default: true)
- ansible_pull_run_once: run ansible pull once during the execution of this role
  (default: true).
- ansible_pull_vault_password: sets a vault password for the ansible run
- ansible_pull_vault_pw_file_location: defines the location of the vault password file

If you use ansible(-core) via pip package manager

- ansible_pip_use: if ansible should be runned via pip, default: false
- ansible_pip_python_version: the version of python that is used to run ansible via pip, default: "standard", example: "3.8" (Only on RHEL 8)
- ansible_pip_ansible_version: ansible pip package version, default: 4.10.0, not that ansible >5.0 package requires python version 3.8

If you use ansible(-core) version >2.9

- ansible_collection_use: if collections should be used, default: false
- ansible_collection_url_file: what kind of collection should be installed prior the run, default: "requirements.yml", example: "git+https://oauth2:<gitlab_access_key>@gitlab.ethz.ch/id-cd-lnx/ansible/id-cd-lnx.git,1.0.1"
- ansible_collection_install_arg: arguments that should be used for the collection install, default: ""

You may also prepare a dedicated user to push configurations later, using priviledge escalation. Variables are:

- ansible_push_user_create: set to true to manage the user; defaults to false
- ansible_push_user_name: defaults to "ansible"
- ansible_push_user_home: home directory (/var/lib/ansible)
- ansible_push_user_sudo: install a configuration in /etc/sudoers.d. Default: true
- ansible_push_user_sudo_perms: permissions for sudo, defaults to "ALL=(ALL) NOPASSWD: ALL"
- ansible_push_user_password: set a password, if desired. Defaults to "" (set no password)
- ansible_push_user_sshkey: provide a ssh public key (empty, no default)
