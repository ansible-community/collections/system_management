---
- name: Install python3 on EL7, not standard
  ansible.builtin.package:
    name: "{{ item }}"
  loop:
    - python3
    - libselinux-python3
  when: ansible_os_family | lower == "redhat" and ansible_distribution_major_version | int == 7

- name: Install custom python version for RedHat systems
  ansible.builtin.package:
    name: "python{{ ansible_pip_python_version | replace('.', '') }}"
  when:
    - ansible_pip_python_version != "standard"
    - ansible_pip_use
    - ansible_os_family | lower == "redhat"
    - ansible_distribution_major_version | int > 7

- name: Ensure required ansible-client packages are installed
  ansible.builtin.package:
    name: "{{ ansible_client_packages }}"
    state: latest

- name: Install ansible
  block:

    - name: Install ansible from OS repository
      ansible.builtin.package:
        name: ansible
      when: not ansible_pip_use

    - name: Install ansible from PyPi if selected
      ansible.builtin.include_tasks: ansible_pip.yml
      when: ansible_pip_use

  when: (ansible_pull_install_postboot or ansible_pull_run_once)

- name: Install Ansible pull script
  block:

    - name: Copies vault password file
      ansible.builtin.template:
        src: templates/vault_pw.txt
        dest: "{{ ansible_pull_vault_pw_file_location }}"
        mode: 0600
      when: ansible_pull_vault_password | length != 0

    - name: Install pull script
      ansible.builtin.template:
        src: templates/ansible_pull.sh.j2
        dest: /usr/local/bin/ansible_pull.sh
        mode: 0755

    - name: Install log formatter
      ansible.builtin.copy:
        src: ansible-logger.py
        dest: /usr/local/bin/ansible-logger.py
        mode: 0755

  when:
    - ansible_pull_repository | length != 0
    - ansible_pull_install_postboot or ansible_pull_run_once or ansible_pull_install_cronjob

- name: Install Ansible post boot execution
  ansible.builtin.template:
    src: templates/ansible_postboot.service.j2
    dest: /etc/systemd/system/ansible_postboot.service
    mode: "0644"
  when:
    - ansible_pull_repository | length != 0
    - ansible_pull_install_postboot or ansible_pull_run_once

- name: Activate Ansible post boot execution
  ansible.builtin.systemd:
    name: ansible_postboot
    enabled: true
    daemon_reload: true
  when:
    - ansible_pull_install_postboot
    - ansible_pull_repository | length != 0

- name: Get service facts
  ansible.builtin.service_facts:

- name: Deactivate Ansible post boot execution
  ansible.builtin.systemd:
    name: ansible_postboot
    enabled: false
    daemon_reload: true
  when: not ansible_pull_install_postboot and 'ansible_postboot.service' in ansible_facts.services

- name: Install Ansible pull cronjob
  ansible.builtin.template:
    src: templates/ansible_pull.j2
    dest: /etc/cron.d/ansible_pull
    mode: 0600
  when:
    - ansible_pull_install_cronjob
    - ansible_pull_repository | length != 0

- name: Uninstall Ansible pull cronjob
  ansible.builtin.file:
    path: /etc/cron.d/ansible_pull
    state: absent
  when:
    - not ansible_pull_install_cronjob

- name: Run Ansible playbook once
  ansible.builtin.systemd:
    name: ansible_postboot
    state: restarted
    daemon_reload: true
  when:
    - ansible_pull_run_once
    - ansible_pull_repository | length != 0

- name: Create ansible user
  ansible.builtin.include_tasks: ansible_user.yml
  when: ansible_push_user_create
