#!/usr/bin/python2
import sys
import json
import logging, logging.handlers

FORMAT='%(asctime)-15s %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger("ansible")
handler = logging.handlers.SysLogHandler(address = "/dev/log")
logger.addHandler(handler)

ansible_log = sys.stdin
logitems = json.load(ansible_log)

failed_tasks = []
changed_tasks = []

for play in logitems["plays"]:
    for task in play["tasks"]:
        for host in task["hosts"]:
            # for item in task["hosts"][host]:

            if "failed" in task["hosts"][host].keys() and task["hosts"][host]["failed"] == True:
                failed_tasks.append(task)

            if "changed" in task["hosts"][host].keys() and task["hosts"][host]["changed"] == True:
                changed_tasks.append(task)

if len(failed_tasks) > 0:
    logger.setLevel(logging.ERROR)
    print("Failed Tasks")
    print("------------")
    for task in failed_tasks:
        logger.error("ansible-cron: %s", "failed: %s" % task["task"]["name"])

if len(changed_tasks) > 0:
    logger.setLevel(logging.WARN)
    print("Changed Tasks")
    print("-------------")
    for task in changed_tasks:
        
        logger.warning("ansible-cron: %s", "changed: %s" % task["task"]["name"])
