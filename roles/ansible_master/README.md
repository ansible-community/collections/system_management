ansible_master
=========

Install an up to date Ansible for a controller host

Tested on RHEL8, Ubuntu 20.04 and Debian Buster

Requirements
------------

No special requirements.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: ansible_controller
      roles:
         - ethz.system_management.ansible_master

System Modifications
--------------------

This role replaces the standard Ansible provided by OS packages. It is meant to be a workaround as
long as the OS packages provide the outdated 2.9 versions. Only newer versions are able to install
collections from git sources.

On RHEL, also `pip` is replaced by the version from PyPi, as the latest version from the OS repository
is not able to install Ansible from PyPi.

License
-------

BSD

